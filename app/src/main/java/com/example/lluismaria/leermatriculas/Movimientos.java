package com.example.lluismaria.leermatriculas;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Movimientos extends FragmentActivity implements OnMapReadyCallback {

    private FirebaseDatabase database;
    private DatabaseReference myRefMatricula;
    private DatabaseReference myRefUbicacion;
    private ValueEventListener childEventListenerMatricula;
    private ArrayList<String> llistaMatricules = new ArrayList();
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        obtenerUbicacion();
    }

    public void obtenerUbicacion() {
        database = FirebaseDatabase.getInstance();

        myRefMatricula = database.getReference("root");
        String test = myRefMatricula.getKey();
        // test val: ubicacion
        this.childEventListenerMatricula = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot matriculaSnapshot : dataSnapshot.getChildren()) {
                    llistaMatricules.add(matriculaSnapshot.getKey());
                }
                for (int i = 0; i < llistaMatricules.size(); i++) {
                    buscarUbicacionPorMatricula(llistaMatricules.get(i));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        myRefMatricula.addListenerForSingleValueEvent(childEventListenerMatricula);
    }

    private void buscarUbicacionPorMatricula(String s) {
        Log.i("buscarUbicacion", "buscarUbicacionPorMatricula");

        myRefUbicacion = database.getReference("root").child(s);
        Query lastQuery = myRefUbicacion.limitToLast(1);
        lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Ubicacion last = null;
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    last = child.getValue(Ubicacion.class);
                }
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(last.latitud, last.longitud))
                        .title(dataSnapshot.getKey())
                );
                Log.i("onChildAdded", "ubicacionEncontrada");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        {

        }
    }
}
