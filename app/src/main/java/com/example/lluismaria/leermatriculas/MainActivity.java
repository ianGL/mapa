package com.example.lluismaria.leermatriculas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seleccion);
        Button boton1 = findViewById(R.id.posicion);
        Button boton2 = findViewById(R.id.trabajadores);
        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Movimientos.class);
                MainActivity.this.startActivity(intent);
            }
        });
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent workers = new Intent(MainActivity.this, Trabajadores.class);
                MainActivity.this.startActivity(workers);
            }
        });
    }
}
